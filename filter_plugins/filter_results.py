import requests

def my_ip(url):
    cidr = "{0}/32".format(str(requests.get(url).text))
    return cidr

def parse_results(results, key="id", tagged=True):
    """
    Args:
        results (list): List of dictionaries of the instances that were created.

    Basic Usage:
        >>> results = []
        >>> instance_ids = parse_results(results)

    Returns:
        List of vpc subnet ids
    """
    # return an attribute for all subnets that match
    instances = []
    def get_instance_id(item):
        list_to_iterate = None
        if tagged:
            list_to_iterate = item['tagged_instances']
        else:
            list_to_iterate = item['instances']

        for data in list_to_iterate:
            instance_id = data.get(key, None)
            if instance_id:
                instances.append(instance_id)

    if isinstance(results, list):
        for item in results:
            get_instance_id(item)

    elif isinstance(results, dict):
        get_instance_id(results)

    return list(set(instances))

def parse_results_by_az(results, az, key="id"):
    """
    Args:
        results (list): List of dictionaries of the instances that were created.
        az (str): The availability zone of the instance you want.
        key (str): Valid key in the results dictionary

    Basic Usage:
        >>> results = []
        >>> instance_ids = parse_results_by_az(results, "eu-west-1a")

    Returns:
        List of vpc subnet ids
    """
    # return an attribute for all subnets that match
    instances = []
    def get_instance_id(item):
        for data in item['tagged_instances']:
            instance_id = data.get(key, None)
            instance_az = data.get("placement", None)
            if instance_id and instance_az == az:
                instances.append(instance_id)

    if isinstance(results, list):
        for item in results:
            get_instance_id(item)

    elif isinstance(results, dict):
        get_instance_id(results)

    return list(set(instances))

def parse_subnets_by_tag(subnets, tag_key, tag_value, return_key='id',
                         return_count=True):
    """
    Args:
        subnets (list): List of dictionaries of the subnets that were created.
        tag_key (str): The tag key you are searching by.
        tag_value (str): The value of the tag you want to search by.

    Kwargs:
        return_key (str): The key you want returned.

    Basic Usage:
        >>> subnets = [
            {
                "az": "eu-west-1a",
                "cidr": "10.1.0.0/24",
                "id": "subnet-f6275193",
                "resource_tags": {
                    "Environment": "dev",
                    "Name": "dev_public",
                    "Tier": "public"
                }
            },
            {
                "az": "eu-west-1a",
                "cidr": "10.1.100.0/24",
                "id": "subnet-f1275194",
                "resource_tags": {
                    "Environment": "dev",
                    "Name": "dev_private",
                    "Tier": "private"
                }
            }
        ]
        >>> tag_key = "Name"
        >>> tag_value = "Development Private"
        >>> subnet_ids = parse_subnets_by_tag(subnets, tag_key, tag_value)

    Returns:
        List of vpc subnet ids
    """
    # return an attribute for all subnets that match
    subnet_values = []
    for item in subnets:
        for key, value in item['resource_tags'].iteritems():
            if key == tag_key and value == tag_value:
                subnet_values.append(item[return_key])
    subnet_values.sort()
    new_values = []
    i = 0
    if return_count:
        for value in subnet_values:
            new_values.append({"count": str(i), return_key: value})
            i += 1
    else:
        new_values = subnet_values
    return new_values


class FilterModule(object):
    ''' Ansible core jinja2 filters '''

    def filters(self):
        return {
            'parse_subnets_by_tag': parse_subnets_by_tag,
            'parse_results_by_az': parse_results_by_az,
            'parse_results': parse_results,
            'my_ip': my_ip,
        }
