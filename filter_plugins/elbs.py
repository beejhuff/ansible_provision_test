from boto.ec2.elb import connect_to_region

def get_ec2_instance_ids_by_elb(name, elb_region):
    """
    Args:
        name (str): The name of the elb you are retrieving the instance ids for.
        elb_region (str): The region in which the elb resides.

    Basic Usage:
        >>> instance_ids = get_ec2_instance_ids_by_elb("test1", "eu-west-1")
        ['i-3916a295', u'i-da55527b', u'i-e37e905b']

    Returns:
        List of ec2 instance ids
    """
    instance_ids = []
    connect = connect_to_region(elb_region)
    elbs_in_region = connect.get_all_load_balancers()
    for elb in elbs_in_region:
        if elb.name == name:
            instance_ids = map(lambda x: x.id, elb.instances)
    return instance_ids

def return_previous_instances(name, elb_region, new_instances):
    """
    Args:
        name (str): The name of the elb you are retrieving the instance ids for.
        elb_region (str): The region in which the elb resides.
        new_instances (list): List of new ec2 instance ids

    Basic Usage:
        >>> previous_instances = return_previous_instances("test1", "eu-west-1")
        ['i-3916a295', u'i-da55527b', u'i-e37e905b']

    Returns:
        List of ec2 instance ids
    """
    instance_ids = []
    previous_instance_ids = []
    connect = connect_to_region(elb_region)
    elbs_in_region = connect.get_all_load_balancers()
    for elb in elbs_in_region:
        if elb.name == name:
            instance_ids = map(lambda x: x.id, elb.instances)
            previous_instance_ids = (
                list(set(instance_ids).difference(new_instances))
            )
    return previous_instance_ids

def get_elb_dns_name(name, elb_region):
    """
    Args:
        name (str): The name of the elb you are retrieving the instance ids for.
        elb_region (str): The region in which the elb resides.

    Basic Usage:
        >>> elb_dns_name = get_elb_dns_name("test1", "eu-west-1")

    Returns:
        String
    """
    dns_name = None
    connect = connect_to_region(elb_region)
    elbs_in_region = connect.get_all_load_balancers()
    for elb in elbs_in_region:
        if elb.name == name:
            dns_name = elb.dns_name
    return dns_name


class FilterModule(object):
    ''' Ansible core jinja2 filters '''

    def filters(self):
        return {
            'get_ec2_instance_ids_by_elb': get_ec2_instance_ids_by_elb,
            'get_elb_dns_name': get_elb_dns_name,
            'return_previous_instances': return_previous_instances,
        }
